<?php
require('vendor/autoload.php');
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use App\Updater;

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$pdo = new PDO('mysql:host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD'));
$fpdo = new FluentPDO($pdo);
$fpdo->debug = false;

Updater::init($fpdo);

$client = new Client([
    'headers' => [
        'User-Agent' => 'Sia-Agent',
    ]
]);

$res = $client->request('GET', getenv('SIAD').'/consensus');
$consensus = json_decode($res->getBody());

$last_height = $fpdo->from('block_hash_index')
                    ->select('height')->orderBy('height desc')->limit(1);
$last_height = $last_height->fetch();
if (empty($last_height['height'])) {
    $last_height = 10;
} else {
    $last_height = $last_height['height'];
}

$times = []; //Some stats
$slowest = [];
$fastest = [];

echo $last_height.PHP_EOL;

for ($i = $last_height - 1; $i <= $consensus->height; $i++) {
    //myg
    $block_start = microtime(true);
    $res = $client->request('GET', getenv('SIAD').'/consensus/blocks?height='.$i);
    $json = json_decode($res->getBody());

    // echo json_encode($json).PHP_EOL.PHP_EOL.PHP_EOL;

    $height = $json->height;
    $prev_height = $json->height-1;
    $prev_block_hash = $json->parentid;
    $block_hash = $json->id;

    Updater::addHash($json->id, 'blockid', $height);

    foreach ($json->minerpayouts as $outputid => $output) {
        Updater::addHash($output->unlockhash, 'unlockhash', $height);
    }

    foreach ($json->transactions as $transactionid => $transaction) {
        Updater::addHash($transaction->id, 'transactionid', $height);

        foreach ($transaction->siacoinoutputs as $scoutputid => $scoutput) {
            Updater::addHash($scoutput->id, 'siacoinoutputid', $height, $scoutput->value);
            Updater::addHash($scoutput->unlockhash, 'unlockhash', $height);
        }

        foreach ($transaction->siacoininputs as $scinoputid => $scinput) {
            Updater::addSpent($scinput->parentid);
            Updater::addHash($scinput->parentid, 'siacoinoutputid', $height);
        }

        foreach ($transaction->filecontracts as $fid => $fc) {
            $filecontractid = $fc->id;
            Updater::addHash($filecontractid, 'filecontractid', $height);
            Updater::addHash($fc->unlockhash, 'unlockhash', $height);

            foreach ($fc->validproofoutputs as $key => $value) {
                Updater::addHash($value->id, 'siacoinoutputid', $height, $value->value);
                Updater::addHash($value->unlockhash, 'unlockhash', $height);
                Updater::addProof($value->id, $filecontractid, $height);
            }
            foreach ($fc->missedproofoutputs as $key => $value) {
                Updater::addHash($value->id, 'siacoinoutputid', $height, $value->value);
                Updater::addHash($value->unlockhash, 'unlockhash', $height);
                Updater::addProof($value->id, $filecontractid, $height);
            }
        }

        foreach ($transaction->filecontractrevisions as $fid => $fc) {
            $filecontractid = $fc->parentid;
            Updater::addHash($filecontractid, 'filecontractid', $height);
            Updater::addHash($fc->newunlockhash, 'unlockhash', $height);

            foreach ($fc->newvalidproofoutputs as $key => $value) {
                Updater::addHash($value->unlockhash, 'unlockhash', $height);
            }
            foreach ($fc->newmissedproofoutputs as $key => $value) {
                Updater::addHash($value->unlockhash, 'unlockhash', $height);
            }
        }

        foreach ($transaction->storageproofs as $scinoputid => $scinoput) {
            Updater::addHash($scinoput->parentid, 'filecontractid', $height);
        }

        foreach ($transaction->siafundoutputs as $sfoutputid => $sfoutput) {
            Updater::addHash($sfoutput->id, 'siafundoutputid', $height, $sfoutput->value);
            Updater::addHash($sfoutput->unlockhash, 'unlockhash', $height);
        }

        foreach ($transaction->siafundinputs as $sfinputid => $sfinput) {
            Updater::addSpent($sfinput->parentid);
            Updater::addHash($sfinput->parentid, 'siafundoutputid', $height);
        }
    }

    $block_end = microtime(true);
    $process_time = $block_end-$block_start;
    echo "Block#{$height} processed in ".round($process_time, 4)."s".PHP_EOL;
    $times[] = $process_time;

    if (empty($slowest['height']) || $process_time > $slowest['time']) {
        $slowest = ['height' => $height, 'time' => round($process_time, 8)];
    }

    if (empty($fastest['height']) || $process_time < $fastest['time']) {
        $fastest = ['height' => $height, 'time' => round($process_time, 8)];
    }
}

Updater::cleanProofPool();
Updater::dumpProofs();

$avg = round(array_sum($times)/count($times), 3);
echo "Avg. process time: {$avg}s".PHP_EOL;
echo "Slowest: {$slowest['height']} -> {$slowest['time']}s".PHP_EOL;
echo "Fastest: {$fastest['height']} -> {$fastest['time']}s".PHP_EOL;
