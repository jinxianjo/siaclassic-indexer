## SiaClassic Indexer

## Related links
- [SiaClassic Explorer](https://gitlab.com/jinxianjo/siaclassic-explorer)
- [SiaClasic Hub](https://gitlab.com/jinxianjo/siaclassic-hub)
- [Laravel 5.4 docs](https://laravel.com/docs/5.4)

## Installation

### Web
- ``git clone https://gitlab.com/jinxianjo/siaclassic-indexer.git``
- import database.sql
- Edit mysql config (.env)
- Edit siad address (localhost:9980)
- run ``php update.php``
- Done!

## License

Available under [the MIT license](http://mths.be/mit).
